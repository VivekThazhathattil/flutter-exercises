Exercises completed:

1. tiktok app home page

2. REST API with Flutter | Step by step tutorial - [CodeX] https://www.youtube.com/watch?v=c09XiwOZKsI

3. Flutter Animation Tutorial | Simple scale transition - [CodeX] https://www.youtube.com/watch?v=29LMMYltyao

4. Let's clone a dribbble design using FLUTTER ♡ - [Mitch Koko] https://www.youtube.com/watch?v=eegl7of4g-o

5. Tinder Style Card Swipe • Flutter Tutorial - [Mitch Koko] https://www.youtube.com/watch?v=MwG7INyr1DE

6. ANIMATED CONTAINER • Flutter Widget of the Day #14 - [Mitch Koko] https://www.youtube.com/watch?v=kWIrhqhCVBk

7. (not completed) Using local_image_provider library to access image files in the device - [local_image_provider pub.dev] https://pub.dev/packages/local_image_provider

8. Flutter in Focus - [Flutterdev, google developers]

## Hair Salon UI

![screenshot 1](.images/hair_salon2.png)
![screenshot 2](.images/hair_salon1.png)

## Flutter in Focus
![screenshot 3](.images/fif1.png)