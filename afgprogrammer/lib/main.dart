import 'package:flutter/material.dart';

import 'animation/fade_animation.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Learning from afgprogrammer',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<List<String>> products = [
    [
      'assets/images/watch-1.jpg',
      'Hugo Boss Oxygen',
      '100 \$',
    ],
    [
      'assets/images/watch-2.jpg',
      'Hugo Boss Signature',
      '120 \$',
    ],
    [
      'assets/images/watch-3.jpg',
      'Casio G-Shock Premium',
      '80 \$',
    ],
  ];

  int currentIndex = 0;

  void _next() {
    setState(() {
      currentIndex = currentIndex == products.length - 1 ? 0 : ++currentIndex;
    });
  }

  void _prev() {
    setState(() {
      currentIndex = currentIndex == 0 ? products.length - 1 : --currentIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: Colors.white,
          child: Column(
            children: [
              GestureDetector(
                onHorizontalDragEnd: (DragEndDetails details) {
                  final xDir = details.velocity.pixelsPerSecond.dx;
                  if (xDir > 0) {
                    _prev();
                  } else if (xDir < 0) {
                    _next();
                  }
                },
                child: FadeAnimation(
                  delay: 0.8,
                  child: Container(
                    width: double.infinity,
                    height: 550,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(products[currentIndex][0]),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomRight,
                          colors: [
                            Colors.grey[700]!.withOpacity(0.9),
                            Colors.grey.withOpacity(0.0),
                          ],
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          FadeAnimation(
                            delay: 1,
                            child: Container(
                              width: 90,
                              margin: EdgeInsets.only(bottom: 60),
                              child: Row(
                                children: _buildIndicator(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Transform.translate(
                  offset: const Offset(0, -40),
                  child: FadeAnimation(
                    delay: 1.0,
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(30.0),
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          )),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FadeAnimation(
                            delay: 1.3,
                            child: Text(
                              products[currentIndex][1],
                              style: TextStyle(
                                color: Colors.grey[800],
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          FadeAnimation(
                            delay: 1.5,
                            child: Row(
                              children: [
                                Icon(Icons.star,
                                    size: 18, color: Colors.yellow[700]),
                                Icon(Icons.star,
                                    size: 18, color: Colors.yellow[700]),
                                Icon(Icons.star,
                                    size: 18, color: Colors.yellow[700]),
                                Icon(Icons.star,
                                    size: 18, color: Colors.yellow[700]),
                                Icon(Icons.star_half,
                                    size: 18, color: Colors.yellow[700]),
                                const SizedBox(
                                  width: 5,
                                ),
                                const Text('4.2 /70 reviews',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12)),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Align(
                                alignment: Alignment.bottomCenter,
                                child: FadeAnimation(
                                  delay: 1.7,
                                  child: Container(
                                    height: 45,
                                    decoration: BoxDecoration(
                                      color: Colors.yellow[700],
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: const Center(
                                        child: Text(
                                      'ADD TO CART',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )),
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  List<Widget> _buildIndicator() {
    List<Widget> indicators = [];
    for (int i = 0; i < products.length; ++i) {
      if (currentIndex == i) {
        indicators.add(_indicator(true));
      } else {
        indicators.add(_indicator(false));
      }
    }
    return indicators;
  }
}

Widget _indicator(bool isActive) {
  return Expanded(
    child: Container(
      height: 4,
      margin: const EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: isActive ? Colors.grey[800] : Colors.white,
      ),
    ),
  );
}
