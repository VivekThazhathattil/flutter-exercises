import 'package:animated_container/pages/homepage.dart';
import 'package:flutter/material.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Animated Container Introduction',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
