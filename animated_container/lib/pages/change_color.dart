import 'package:flutter/material.dart';

class ChangeColor extends StatefulWidget {
  const ChangeColor({super.key});

  @override
  State<ChangeColor> createState() => _ChangeColorState();
}

class _ChangeColorState extends State<ChangeColor> {
  double boxHeight = 100.0, boxWidth = 100.0;
  var _color1 = Colors.deepPurple[200];
  var _color2 = Colors.deepPurple[200];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                _color1 = Colors.deepOrange;
                _color2 = Colors.black;
              });
            },
            child: Center(
              child: AnimatedContainer(
                height: boxHeight,
                width: boxWidth,
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeIn,
                alignment: Alignment.center,
                color: _color1,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                _color1 = Colors.yellow;
                _color2 = Colors.green;
              });
            },
            child: Center(
              child: AnimatedContainer(
                height: boxHeight,
                width: boxWidth,
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeIn,
                alignment: Alignment.center,
                color: _color2,
              ),
            ),
          )
        ],
      ),
    );
  }
}
