import 'package:flutter/material.dart';

class ChangePosition extends StatefulWidget {
  final BuildContext prevContext;
  const ChangePosition({super.key, required this.prevContext});

  @override
  State<ChangePosition> createState() => _ChangePositionState();
}

class _ChangePositionState extends State<ChangePosition> {
  final _animDuration = const Duration(milliseconds: 500);
  var _boxBottomHeight, _boxTopHeight, _width;

  @override
  void initState() {
    _boxBottomHeight = MediaQuery.of(widget.prevContext).size.height / 4;
    _boxTopHeight = MediaQuery.of(widget.prevContext).size.height * 2 / 4;
    _width = MediaQuery.of(widget.prevContext).size.width;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    _boxBottomHeight =
                        MediaQuery.of(widget.prevContext).size.height * 1 / 4;
                    _boxTopHeight =
                        MediaQuery.of(widget.prevContext).size.height * 2 / 4;
                  });
                },
                child: Center(
                  child: AnimatedContainer(
                    height: _boxTopHeight,
                    duration: _animDuration,
                    curve: Curves.easeIn,
                    alignment: Alignment.center,
                    color: Colors.red,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    _boxBottomHeight =
                        MediaQuery.of(widget.prevContext).size.height * 2 / 4;
                    _boxTopHeight =
                        MediaQuery.of(widget.prevContext).size.height / 4;
                  });
                },
                child: Center(
                  child: AnimatedContainer(
                    height: _boxBottomHeight,
                    duration: _animDuration,
                    curve: Curves.easeIn,
                    alignment: Alignment.center,
                    color: Colors.black,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
