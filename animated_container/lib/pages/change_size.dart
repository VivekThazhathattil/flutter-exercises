import 'package:flutter/material.dart';

class ChangeSize extends StatefulWidget {
  const ChangeSize({super.key});

  @override
  State<ChangeSize> createState() => _ChangeSizeState();
}

class _ChangeSizeState extends State<ChangeSize> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          AnimatedContainer(
            duration: const Duration(milliseconds: 500),
            alignment: Alignment.center,
            color: Colors.deepPurple[200],
          )
        ],
      ),
    );
  }
}
