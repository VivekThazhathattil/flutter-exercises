import 'package:animated_container/utils/homepage_button.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            HomePageButton(buttonText: 'Change size'),
            HomePageButton(buttonText: 'Change color'),
            HomePageButton(buttonText: 'Change position'),
          ],
        ),
      ),
    );
  }
}
