import 'package:animated_container/pages/change_size.dart';
import 'package:animated_container/pages/change_color.dart';
import 'package:animated_container/pages/change_position.dart';
import 'package:flutter/material.dart';

class HomePageButton extends StatelessWidget {
  final String buttonText;
  const HomePageButton({super.key, required this.buttonText});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 2,
      height: MediaQuery.of(context).size.height / 8,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ElevatedButton(
          style: ButtonStyle(
            elevation: MaterialStateProperty.all(2),
            padding: MaterialStateProperty.all(const EdgeInsets.all(24.0)),
          ),
          onPressed: () {
            if (buttonText == 'Change size') {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const ChangeSize()));
            }
            switch (buttonText) {
              case 'Change size':
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const ChangeSize()));
                break;
              case 'Change color':
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const ChangeColor()));
                break;
              case 'Change position':
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ChangePosition(
                          prevContext: context,
                        )));
                break;
            }
          },
          child: Text(
            buttonText,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
        ),
      ),
    );
  }
}
