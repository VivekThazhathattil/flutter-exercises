import 'package:dribble_design_flutter1/utils/emoticon_face.dart';
import 'package:dribble_design_flutter1/utils/exercise_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dribble UI Design',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        bottomNavigationBar: BottomNavigationBar(items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'),
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'),
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'),
        ]),
        body: Container(
            color: Colors.blue[700],
            child: SafeArea(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Column(
                      children: [
                        //Hi Jared!
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Hi, Jared!',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 32.0,
                                      color: Colors.white,
                                    )),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  '23 Jan, 2021',
                                  style: TextStyle(
                                    color: Colors.blue[200],
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            ),
                            // Notification
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.blue[600],
                                  borderRadius: BorderRadius.circular(12)),
                              padding: EdgeInsets.all(12.0),
                              child: Icon(
                                Icons.notifications,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                        // Search bar
                        SizedBox(
                          height: 25,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.blue[600],
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          padding: EdgeInsets.all(12),
                          child: Row(children: [
                            Icon(
                              Icons.search,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Search',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ]),
                        ),
                        // How do you feel?
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'How do you feel?',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            Icon(
                              Icons.more_horiz,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: const [
                            EmoticonFace(emoticonFace: '😀', feeling: 'Happy'),
                            EmoticonFace(emoticonFace: '😞', feeling: 'Sad'),
                            EmoticonFace(emoticonFace: '😠', feeling: 'Angry'),
                            EmoticonFace(
                                emoticonFace: '😖', feeling: 'Confused'),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(24),
                          topRight: Radius.circular(24)),
                      child: Container(
                        color: Colors.grey[300],
                        child: Padding(
                          padding: const EdgeInsets.all(24.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    'Exercises',
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Icon(Icons.more_horiz),
                                ],
                              ),
                              Expanded(
                                child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      children: const [
                                        ExerciseTile(
                                          icon: Icons.speaker_outlined,
                                          iconColor: Colors.purple,
                                          title: 'Speaking Session',
                                          subtitle: 15,
                                        ),
                                        ExerciseTile(
                                          icon: Icons.book_outlined,
                                          iconColor: Colors.red,
                                          title: 'Reading Session',
                                          subtitle: 12,
                                        ),
                                        ExerciseTile(
                                          icon: Icons.play_arrow_outlined,
                                          iconColor: Colors.amber,
                                          title: 'Playing Session',
                                          subtitle: 08,
                                        ),
                                        ExerciseTile(
                                          icon: Icons.flutter_dash,
                                          iconColor: Colors.green,
                                          title: 'Flutter Session',
                                          subtitle: 22,
                                        ),
                                      ],
                                    )),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
