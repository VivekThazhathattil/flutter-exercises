import 'package:flutter/material.dart';

class EmoticonFace extends StatelessWidget {
  final String emoticonFace;
  final String feeling;
  const EmoticonFace(
      {super.key, required this.emoticonFace, required this.feeling});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
                color: Colors.blue[600],
                borderRadius: BorderRadius.circular(12)),
            padding: const EdgeInsets.all(12.0),
            child: Text(
              emoticonFace,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            )),
        const SizedBox(
          height: 5,
        ),
        Text(
          feeling,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 10,
          ),
        )
      ],
    );
  }
}
