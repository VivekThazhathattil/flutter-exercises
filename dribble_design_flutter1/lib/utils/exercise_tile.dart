import 'package:flutter/material.dart';

class ExerciseTile extends StatelessWidget {
  final iconColor;
  final icon;
  final String title;
  final int subtitle;
  const ExerciseTile(
      {super.key,
      required this.iconColor,
      required this.icon,
      required this.title,
      required this.subtitle});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
          margin: EdgeInsets.only(top: 8.0),
          padding: EdgeInsets.all(18.0),
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //exercise Icon
                  ClipRRect(
                    borderRadius: BorderRadius.circular(24.0),
                    child: Container(
                      color: iconColor,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          icon,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  // Title & subtitle
                  Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w600),
                        ),
                        Text(
                          '${subtitle} Exercises',
                          style: TextStyle(fontSize: 12, color: Colors.grey),
                        )
                      ]),
                ],
              ),
              // more horiz
              Icon(Icons.more_horiz),
            ],
          )),
    );
  }
}
