import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dribble UI practice',
      debugShowCheckedModeBanner: false,
      home: Screen1(),
    );
  }
}

class Screen1 extends StatefulWidget {
  const Screen1({super.key});

  @override
  State<Screen1> createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<String> days = ['Mon', 'Tue', 'Wed', "Thu", 'Fri', 'Sat', 'Sun'];
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // carousel
              const Expanded(
                flex: 1,
                child: Carousels(),
              ),
              Expanded(
                // Users in the last week
                flex: 2,
                child: Container(
                    color: Colors.yellow,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //users text
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text(
                                'User in The Last Week',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text('+3,2%',
                                  style: TextStyle(
                                    fontSize: 32,
                                    fontWeight: FontWeight.w700,
                                  )),
                            ],
                          ),
                          //percentage change text
                          // Row with first item being the vertical scale markings
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: Colors.amber,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  // Column for vertical scale markings
                                  Column(
                                    children: const [
                                      Text('40 K'),
                                      Text('20 K'),
                                      Text('10 K'),
                                    ],
                                  ),
                                  //column for stats display
                                  Column(
                                    children: [
                                      // column item 1 - Row bar graph widgets
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: const [
                                          StatBar(),
                                          StatBar(),
                                          StatBar(),
                                          StatBar(),
                                        ],
                                      ),
                                      // column item 2 - Row mon, tue, wed, ... , sun
                                      Row(
                                        children: days
                                            .map((day) => Text(day))
                                            .toList(),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
              Expanded(
                  // Last orders
                  flex: 2,
                  child: Container(color: Colors.blue)),
              // Bottom navigation bar
            ],
          ),
          Column(
            children: [
              Expanded(
                flex: 1,
                child: Container(),
              ),
              Container(
                  // for bottom navigation bar
                  )
            ],
          )
        ],
      ),
    ));
  }
}

class StatBar extends StatelessWidget {
  const StatBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 10,
      color: Colors.red,
    );
  }
}

class Carousels extends StatelessWidget {
  const Carousels({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: [
        CarouselCard(color: Color(0xFFE1BA59)),
        CarouselCard(color: Color(0xFFF6D78B)),
        CarouselCard(color: Color(0xFFA9AEFF)),
      ],
    );
  }
}

class CarouselCard extends StatelessWidget {
  const CarouselCard({
    Key? key,
    required this.color,
  }) : super(key: key);

  final Color color;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Card(
      color: color,
      elevation: 1,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
      child: Container(
        width: size.width * 0.8,
      ),
    );
  }
}
