import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FancyButton extends StatelessWidget {
  final GestureTapCallback onPressed;
  const FancyButton({super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
        fillColor: Colors.deepOrange,
        splashColor: Colors.orange,
        onPressed: onPressed,
        shape: const StadiumBorder(),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: const [
              RotatedBox(
                quarterTurns: 3,
                child: Icon(
                  Icons.explore,
                  color: Colors.orange,
                ),
              ),
              SizedBox(
                width: 8.0,
              ),
              Text(
                'PURCHASE',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ));
  }
}
