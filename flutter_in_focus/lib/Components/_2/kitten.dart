class Kitten {
  final String imageUrl;
  final int age;
  final String description;
  final String name;

  const Kitten(
      {required this.name,
      this.description = '',
      required this.age,
      required this.imageUrl});
}
