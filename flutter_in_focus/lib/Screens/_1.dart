import 'package:flutter/material.dart';
import 'package:flutter_in_focus/Components/_1/fancy_button.dart';

class Episode1 extends StatelessWidget {
  final String title;
  const Episode1({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      floatingActionButton: FancyButton(
        onPressed: () {},
      ),
      body: Container(),
    );
  }
}
