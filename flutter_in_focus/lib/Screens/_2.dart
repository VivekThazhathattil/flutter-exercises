import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_in_focus/Components/_2/kitten.dart';

// Go through material component widgets to add more tools to your toolkit

final String server =
    defaultTargetPlatform == TargetPlatform.android ? '10.0.2.2' : 'localhost';

final List<Kitten> _kittens = <Kitten>[
  Kitten(
    name: 'Mittens',
    description:
        'The pinnacle of cats. When Mitten sits on your lap, you feel like royalty',
    age: 11,
    imageUrl: 'http://$server:8000/kitten0.jpg',
  ),
  Kitten(
    name: 'Fluffy',
    description: 'World\'s cutest Kitten. Seriously, we did the research',
    age: 12,
    imageUrl: 'http://$server:8000/kitten1.jpg',
  ),
  Kitten(
    name: 'Scooter',
    description: 'Chases string faster than 9/10 competing kittens.',
    age: 5,
    imageUrl: 'http://$server:8000/kitten2.jpg',
  ),
  Kitten(
    name: 'Steve',
    description: 'Steve is cool and just kinda hangs out',
    age: 6,
    imageUrl: 'http://$server:8000/kitten3.jpg',
  ),
  Kitten(
    name: 'Dhrishtadyumna',
    description: 'He rose from the flames of yajna to kill his enemy Drona.',
    age: 7,
    imageUrl: 'http://$server:8000/kitten4.jpg',
  ),
  Kitten(
    name: 'Sheryl',
    description: 'Mystic koala',
    age: 8,
    imageUrl: 'http://$server:8000/kitten5.jpg',
  ),
];

class Episode2 extends StatelessWidget {
  const Episode2({super.key});

  Widget _dialogBuilder(BuildContext context, Kitten kitten) {
    ThemeData localTheme = Theme.of(context);
    return SimpleDialog(
      contentPadding: const EdgeInsets.all(0.0),
      children: [
        Image.network(
          kitten.imageUrl,
          fit: BoxFit.fill,
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                kitten.name,
                style: localTheme.textTheme.titleLarge,
              ),
              Text(
                '${kitten.age} months old',
                style: localTheme.textTheme.subtitle2!.copyWith(
                  fontStyle: FontStyle.italic,
                ),
              ),
              const SizedBox(
                height: 16.0,
              ),
              Text(kitten.description, style: localTheme.textTheme.bodyText1),
              const SizedBox(
                height: 16.0,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Wrap(
                  spacing: 7.5,
                  children: [
                    OutlinedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('I\'m ALLERGIC'),
                    ),
                    ElevatedButton(
                      onPressed: () {},
                      child: const Text('ADOPT'),
                    )
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _listItemBuilder(BuildContext context, int index) {
    return GestureDetector(
      onTap: () => showDialog(
          context: context,
          builder: (context) => _dialogBuilder(context, _kittens[index])),
      child: Container(
        padding: const EdgeInsets.only(left: 16.0),
        alignment: Alignment.centerLeft,
        child: Text(
          _kittens[index].name,
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Available Kittens'),
        ),
        body: ListView.builder(
          itemCount: _kittens.length,
          itemExtent: 60.0,
          itemBuilder: _listItemBuilder,
        ));
  }
}
