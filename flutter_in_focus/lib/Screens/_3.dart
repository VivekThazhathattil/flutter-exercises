import 'package:flutter/material.dart';

class Episode3 extends StatelessWidget {
  const Episode3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Yellow Lab'),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          DogName(name: 'Rocky'),
          SizedBox(height: 8.0),
          DogName(name: 'Lyla'),
          SizedBox(height: 8.0),
          DogName(name: 'Nico')
        ],
      )),
    );
  }
}

class DogName extends StatelessWidget {
  final String name;

  const DogName({super.key, required this.name});

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        color: Colors.lightBlueAccent,
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(name),
      ),
    );
  }
}
