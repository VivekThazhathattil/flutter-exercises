import 'package:flutter/material.dart';
import 'package:flutter_in_focus/Screens/_2.dart';
import 'package:flutter_in_focus/Screens/_3.dart';
import 'Screens/_1.dart';

void main() => runApp(const FlutterInFocus());

class FlutterInFocus extends StatelessWidget {
  static List<String> exerciseNames = [
    'Building your first Flutter Widget',
    'Using Material Design with Flutter',
    'How to Create Stateless Widgets - Flutter Widgets 101 Ep. 1',
    'How stateful widgets are used best - Flutter Widgets 101 Ep 2',
    'Inherited Widgets explained - Flutter Widgets 101 Ep 3',
  ];
  const FlutterInFocus({super.key});
  Widget build(BuildContext context) {
    List exercises = [
      Episode1(title: exerciseNames[0]),
      const Episode2(),
      const Episode3(),
    ];
    return MaterialApp(
        title: 'Flutter In Focus Exercises',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.green,
          buttonTheme: const ButtonThemeData(
            textTheme: ButtonTextTheme.primary,
          ),
        ),
        home: Scaffold(
          appBar: AppBar(),
          body: SafeArea(
            child: ListView.builder(
                itemCount: exerciseNames.length,
                itemExtent: 100.0,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => exercises[index]),
                      );
                    },
                    child: Card(
                      elevation: 2.0,
                      child: ListTile(
                        leading: Text(
                          (index + 1).toString(),
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        title: Text(exerciseNames[index],
                            style: Theme.of(context).textTheme.headline6),
                      ),
                    ),
                  );
                }),
          ),
        ));
  }
}
