import 'package:flutter/material.dart';
import 'package:hair_salon_app/res/app_colors.dart';
import 'package:hair_salon_app/res/fonts.dart';
import 'package:hair_salon_app/utils/utilities.dart';

import '../res/images.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: SafeArea(
          child: Column(
            children: [
              verticalSpace(10.0),
              Image.asset(Images.kIconMenu, height: 24),
              verticalSpace(20.0),

              //search bar
              Container(
                width: Utility.screenWidth(context),
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blueGrey.withOpacity(0.1),
                        blurRadius: 20.0,
                        spreadRadius: 10.0,
                        offset: const Offset(0, 10),
                      ),
                    ]),
                child: Row(children: [
                  horizontalSpace(20.0),
                  const Icon(
                    Icons.search,
                    color: AppColors.colorSecondary,
                  ),
                  horizontalSpace(20.0),
                  Expanded(
                    child: TextFormField(
                      cursorColor: AppColors.colorSecondary,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Search',
                        hintStyle: textStyleSubText16px400w,
                        isDense: true,
                      ),
                    ),
                  )
                ]),
              ),
              verticalSpace(20.0),
              Expanded(
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: const [
                    SalonGuy(
                        name: 'John Doe',
                        salonName: 'Bounce Unisex Salon',
                        rating: 4.8,
                        pictureLocation: Images.kImgPlaceHolder1),
                    SalonGuy(
                        name: 'Tracy Harper',
                        salonName: 'Marie Clair Paris Salon',
                        rating: 3.7,
                        pictureLocation: Images.kImgPlaceHolder2),
                    SalonGuy(
                        name: 'Francis Wells',
                        salonName: 'Solastaa Salon',
                        rating: 4.9,
                        pictureLocation: Images.kImgPlaceHolder3),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SalonGuy extends StatelessWidget {
  final String name;
  final String salonName;
  final double rating;
  final String pictureLocation;
  const SalonGuy({
    Key? key,
    required this.name,
    required this.salonName,
    required this.rating,
    required this.pictureLocation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
      margin: const EdgeInsets.symmetric(vertical: 12.0),
      width: Utility.screenWidth(context),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(32.0),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
                style: textStyleRegular18pxW600,
              ),
              verticalSpace(10.0),
              Text(
                salonName,
                style: textStyleSubText14px400w,
              ),
              verticalSpace(10.0),
              Row(
                children: [
                  Image.asset(Images.kIconStar, height: 16.0),
                  horizontalSpace(10.0),
                  Text(rating.toString(), style: textStyleRegular16px500px),
                ],
              ),
              verticalSpace(20.0),
              Container(
                  decoration: BoxDecoration(
                    color: AppColors.colorSecondary,
                    borderRadius: BorderRadius.circular(32.0),
                  ),
                  child: const Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
                    child: Text('View Profile', style: textStyleWhite16px600w),
                  )),
            ],
          ),
          Image.asset(
            pictureLocation,
            height: 100,
            fit: BoxFit.contain,
          ),
        ],
      ),
    );
  }
}
