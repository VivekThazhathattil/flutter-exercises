import 'package:flutter/material.dart';
import 'package:hair_salon_app/Screens/homepage.dart';
import 'package:hair_salon_app/res/app_colors.dart';
import 'package:hair_salon_app/utils/utilities.dart';
import '../res/fonts.dart';
import '../res/images.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              Images.kImgOnboarding,
              height: Utility.screenHeight(context) * 0.5,
            ),
            const Text(
              'Book your favorite \n Hair Stylist',
              style: textStyleDarkHeavy24px800,
            ),
            verticalSpace(20.0),
            const Text(
              'We can help you choose your new favorite hair stylist within seconds!',
              style: textStyleSubText16px500w,
            ),
            verticalSpace(25.0),
            Align(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => HomePage()));
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: AppColors.colorSecondary,
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: const Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 30.0, vertical: 18.0),
                    child: Text(
                      'Register',
                      style: textStyleWhite16px600w,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
