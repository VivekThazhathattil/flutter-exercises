import 'package:flutter/material.dart';
import 'package:hair_salon_app/res/app_colors.dart';

import 'Screens/onboarding_screen.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hair Salon App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: AppColors.whiteMaterialColor,
        scaffoldBackgroundColor: AppColors.colorPrimary,
      ),
      home: OnboardingScreen(),
    );
  }
}
