class Article {
  final String text;
  final String domain;
  final String by;
  final String age;
  final int score;
  final int commentsCount;

  const Article({
    this.text = '',
    this.domain = '',
    this.by = '',
    this.age = '',
    this.score = 0,
    this.commentsCount = 0,
  });
}

const articles = [
  Article(
    text:
        'Circular Shock Acoustic Waves in Ionosphere Triggered by Launch of Formosat-5',
    domain: 'wiley.com',
    by: 'zdw',
    age: '3 hours',
    score: 177,
    commentsCount: 62,
  ),
  Article(
    text: 'BMW says electric car mass production not viable until 2020',
    domain: 'reuters.com',
    by: 'Mononokay',
    age: '2 hours',
    score: 81,
    commentsCount: 128,
  ),
  Article(
    text: 'TCP Tracepoints have arrived in Linux',
    domain: 'brendangregg.com',
    by: 'brendngregg',
    age: '1 hour',
    score: 35,
    commentsCount: 0,
  ),
];
