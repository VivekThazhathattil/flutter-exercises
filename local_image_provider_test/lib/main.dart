import 'package:flutter/material.dart';
import 'package:local_image_provider/local_image_provider.dart' as lip;
void main() => runApp(App());

class App extends StatelessWidget {
  const App({super.key});

  _runFunction() async{
    lip.LocalImageProvider imageProvider = lip.LocalImageProvider();
    bool hasPermission = await imageProvider.initialize();
    if ( hasPermission) {
        List<lip.LocalImage> images = await imageProvider.findLatest(10);
        images.forEach((image) => print( image.id));
    }
    else {
        print("The user has denied access to images on their device.");
    }
  }

  @override
  Widget build(BuildContext context) {
    _runFunction();
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Test local image provider package',
        home: Scaffold(appBar: AppBar(), body: Container()));
  }
}
