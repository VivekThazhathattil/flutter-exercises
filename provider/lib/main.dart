import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: EligibilityScreen(),
      ),
    );
  }
}

class EligibilityScreen extends StatelessWidget {
  EligibilityScreen({super.key});
  final ageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<EligibilityNotifier>(
        create: (context) => EligibilityNotifier(),
        child: Builder(builder: (context) {
          return Container(
              padding: const EdgeInsets.all(16),
              child: Form(child: Consumer<EligibilityNotifier>(
                  builder: (context, provider, child) {
                return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: (provider.isEligible == null)
                              ? Colors.orangeAccent
                              : provider.isEligible
                                  ? Colors.greenAccent
                                  : Colors.redAccent,
                        ),
                        height: 50,
                        width: 50,
                      ),
                      const SizedBox(height: 16),
                      TextFormField(
                        controller: ageController,
                        decoration: const InputDecoration(
                          hintText: 'Give your age',
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Container(
                        width: double.infinity,
                        child: MaterialButton(
                          onPressed: () {
                            final int age =
                                int.parse(ageController.text.trim());
                            provider.checkEligibility(age);
                          },
                          child: const Text('Check'),
                          color: Colors.blue,
                          textColor: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Text(provider.eligibilityMessage),
                    ]);
              })));
        }));
  }
}

class EligibilityNotifier extends ChangeNotifier {
  String _eligibilityMessage = '';
  bool _isEligible = false;

  void checkEligibility(int age) {
    if (age >= 18) {
      eligibleForLicense();
    } else {
      notEligibleForLicense();
    }
  }

  void eligibleForLicense() {
    _eligibilityMessage = 'You are eligible for Driving License';
    _isEligible = true;
    notifyListeners();
  }

  void notEligibleForLicense() {
    _eligibilityMessage = 'You are not eligible for Driving License';
    _isEligible = false;
    notifyListeners();
  }

  String get eligibilityMessage => _eligibilityMessage;
  bool get isEligible => _isEligible;
}
