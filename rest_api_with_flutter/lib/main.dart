import 'package:flutter/material.dart';
import 'package:rest_api_with_flutter/views/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.dark,
      ),
      home: const HomePage(),
    );
  }
}


// -------------------------------------- //
// Important links: 
// ------------------
// Tutorial: https://www.youtube.com/watch?v=c09XiwOZKsI
// json quick type: https://app.quicktype.io/
// json fake data: https://jsonplaceholder.typicode.com/posts
//
//
//
// -------------------------------------- //