import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return HomePage();
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  AnimationController? _animationController;
  late Animation<double> _animation;
  bool _isAnimating = false;

  _toggleAnimation() {
    if (_isAnimating) {
      _animationController!.stop();
    } else {
      _animationController!.forward();
    }
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _animation = Tween<double>(begin: 1.0, end: 1.2).animate(
        CurvedAnimation(parent: _animationController!, curve: Curves.easeIn));
    _animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _animationController!.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _animationController!.forward();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Simple scale animation - thumping heart',
        theme: ThemeData(
          primarySwatch: Colors.brown,
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Thumping heart'),
            ),
            floatingActionButton: FloatingActionButton(
              child: Icon(_isAnimating ? Icons.pause : Icons.play_arrow),
              onPressed: () {
                setState(() {
                  _toggleAnimation();
                  _isAnimating = !_isAnimating;
                });
              },
            ),
            body: ScaleTransition(
              scale: _animation,
              child: Center(
                child: SizedBox(
                  width: 100,
                  child: Image.asset(
                    'lib/assets/heart.png',
                  ),
                ),
              ),
            )));
  }
}
