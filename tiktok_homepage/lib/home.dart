import 'package:flutter/material.dart';
import 'package:tiktoklikescroller/tiktoklikescroller.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List user = [
    {
      'name': "@abderrahmane",
      'description': 'Hello every body',
      'image': 'img/1.jpg',
      'like': '110.9M',
      'comment': '100.3K',
      'share': '1.5M',
      'music_Image': 'img/11.jpg'
    },
    {
      'name': "@abderrahmane",
      'description': 'Hello every to my new tiktok',
      'image': 'img/1.jpg',
      'like': '10.9M',
      'comment': '10.3K',
      'share': '1M',
      'music_Image': 'img/11.jpg'
    },
    {
      'name': "@abderrahmane",
      'description': 'Hello every this is new instagrame',
      'image': 'img/1.jpg',
      'like': '110.9M',
      'comment': '100.3K',
      'share': '1.5M',
      'music_Image': 'img/11.jpg'
    },
    {
      'name': "@abderrahmane",
      'description': 'Hello every body',
      'image': 'img/1.jpg',
      'like': '110.9M',
      'comment': '100.3K',
      'share': '1.5M',
      'music_Image': 'img/11.jpg'
    },
    {
      'name': "@abderrahmane",
      'description': 'Hello every body',
      'image': 'img/1.jpg',
      'like': '110.9M',
      'comment': '100.3K',
      'share': '1.5M',
      'music_Image': 'img/11.jpg'
    },
    {
      'name': "@hmida",
      'description':
          ' ezfze fzefez fezfezf zefze foefoefze fezf ze f ezfzef zefze fezf ezfze fzefez fezfezf zefze foefoefze fezf ze f ezfzef zefze fezf ezfze fzefez fezfezf zefze f...',
      'image': 'img/1.jpg',
      'like': '11.9M',
      'comment': '10.3K',
      'share': '500.2k',
      'music_Image': 'img/11.jpg'
    }
  ];

  static final List<Color> colorsStencil = <Color>[
    Colors.red,
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.red,
    Colors.blue,
    Colors.green,
    Colors.orange,
  ];

  final List<Color> colors = <Color>[
    ...colorsStencil,
    ...colorsStencil,
    ...colorsStencil
  ];

  var like = 0;

  @override
  Widget build(BuildContext context) {
    var hei = MediaQuery.of(context).size.height;
    var wid = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Stack(
        children: [
          TikTokStyleFullPageScroller(
            contentSize: user.length,
            swipePositionThreshold: 0.2,
            swipeVelocityThreshold: 2000,
            animationDuration: const Duration(milliseconds: 300),
            builder: (BuildContext context, int index) {
              return Stack(
                children: [
                  Container(
                      color: colors[index],
                      child: Center(
                          child: Text(
                        "$index",
                        key: Key('$index-text'),
                        style: TextStyle(fontSize: 48, color: Colors.white),
                      ))),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        color: Colors.black26,
                        margin: EdgeInsets.only(bottom: 25, left: 10),
                        alignment: Alignment.bottomLeft,
                        width: wid - 110,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              user[index]['name'],
                              style: const TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Container(
                              child: Text(
                                user[index]['description'],
                                style: const TextStyle(
                                  fontSize: 13,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
