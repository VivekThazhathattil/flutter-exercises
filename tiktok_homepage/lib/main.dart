import 'package:flutter/material.dart';
import 'home.dart';

void main() => runApp(TikTokHomePage());

class TikTokHomePage extends StatelessWidget {
  const TikTokHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "TikTok Homepage",
      home: Home(),
    );
  }
}
