import 'package:flutter/material.dart';
import 'package:tinder_swipe/utils/swipe_set.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(child: SwipeSet()));
  }
}
