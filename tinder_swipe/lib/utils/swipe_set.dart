import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:swipe_cards/swipe_cards.dart';

class SwipeSet extends StatefulWidget {
  const SwipeSet({super.key});

  @override
  State<SwipeSet> createState() => _SwipeSetState();
}

class _SwipeSetState extends State<SwipeSet> {
  final List<String> _names = ['Red', 'Blue', 'Green', 'Yellow', 'Orange'];
  final List<Color> _colors = [
    Colors.red,
    Colors.blue,
    Colors.green,
    Colors.yellow,
    Colors.orange
  ];
  final List<SwipeItem> _swipeItems = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  MatchEngine _matchEngine = MatchEngine();

  @override
  void initState() {
    for (int i = 0; i < _names.length; ++i) {
      _swipeItems.add(SwipeItem(
          content: Container(
        color: _colors[i],
        child: Text(_names[i]),
      )));
    }
    _matchEngine = MatchEngine(swipeItems: _swipeItems);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SwipeCards(
        matchEngine: _matchEngine,
        onStackFinished: () {},
        itemBuilder: (context, index) {
          return Container(
              alignment: Alignment.center,
              color: _colors[index],
              child: Text(_names[index],
                  style: TextStyle(
                    fontSize: 20,
                  )));
        });
  }
}
