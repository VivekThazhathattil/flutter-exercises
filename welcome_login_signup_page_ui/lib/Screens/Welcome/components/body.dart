import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:welcome_login_signup_page_ui/Screens/Login/login.dart';
import 'package:welcome_login_signup_page_ui/Screens/Welcome/components/rounded_button.dart';
import 'package:welcome_login_signup_page_ui/constants.dart';
import 'background.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
        childWidget: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Welcome to EDU'),
          SvgPicture.asset(
            'assets/icons/chat.svg',
            height: size.height * 0.45,
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          RoundedButton(
              textString: 'LOGIN',
              press: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Login()));
              }),
          RoundedButton(
            textString: 'SIGNUP',
            bgColor: kPrimaryLightColor,
            textColor: kPrimaryColor,
            press: () {},
          ),
        ],
      ),
    ));
  }
}
