import 'package:flutter/material.dart';
import 'package:welcome_login_signup_page_ui/constants.dart';

class RoundedButton extends StatelessWidget {
  final textColor, bgColor;
  final String textString;
  final Function press;
  const RoundedButton({
    Key? key,
    this.textColor = Colors.white,
    this.bgColor = kPrimaryColor,
    required this.press,
    required this.textString,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Container(
        width: size.width * 0.7,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(29),
          child: TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(bgColor),
              foregroundColor: MaterialStateProperty.all(textColor),
              padding: MaterialStateProperty.all(
                  EdgeInsets.symmetric(vertical: 20, horizontal: 40)),
            ),
            onPressed: () {
              press;
            },
            child: Text(textString),
          ),
        ),
      ),
    );
  }
}
